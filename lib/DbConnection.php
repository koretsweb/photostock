<?php
// Singleton
class DbConnection
{
    private static $instance;
    /**
     * @var PDO
     */
    private $pdo;
    public function __construct()
    {
        $this->pdo = new PDO('mysql: host=localhost; dbname=photo_db', 'root');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->query('SET NAMES utf8');
    }
    private function __clone(){}
    private function __wakeUp(){}
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new DbConnection();
        }
        return self::$instance;
    }
    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}