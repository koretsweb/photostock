<?php

class IndexController extends Controller
{
    public function indexAction(Request $request)
    {
        $lasts = new PhotoModel();
        $args = $lasts->showLasts();
        return $this->render('index',$args);
    }
    public function contactAction(Request $request)
    {
         return $this->render('contact');
    }

    public function addAction(Request $request)
    {

        $add = new AddPhoto();
        $add->add();
        return $this->render('add');
    }

    public function search()
    {
        if ($_POST) {
            $str = $_POST['search'];
            $search = new Search($str);
            $args = $search->search($str);
            
            return $this->render('search', $args);
        }

    }

    public function registerAction(Request $request)
    {
        $register = new Register();
        $register->register();
        return $this->render('register');
    }
}