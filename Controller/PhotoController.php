<?php

class PhotoController extends Controller
{
    public function indexAction(Request $request)
    {
        $photos = new PhotoModel();
        $args = $photos->showAll();
        $tags = $photos->randomTags();
        return $this->render('index',$args, $tags);
    }

    public function categoryAction(Request $request)
    {
        $cat = $_GET['route'];
        $cat = explode('/', $cat);  //cat[2] - category
        $photos = new PhotoModel();
        $args = $photos->showCat($cat);
        $tags = $photos->randomTags();
        return $this->render('category',$args, $tags);
        
    }

    public function showAction(Request $request)
    {
        $id = $_GET['route'];
        $id = explode('/', $id);
        $id = $id[2];
        $id = explode('=', $id);
        $id = $id[1];
        $photo = new PhotoModel();
        $args = $photo->showOne($id);
        $tags = $photo->randomTags();
        return $this->render('showOne',$args, $tags);
    }
    public function criterionAction(Request $request)
    {
        $criterion = $_GET['route'];
        $criterion = explode('/', $criterion);
        $criterion = $criterion[2];
        $photo = new PhotoModel();
        if ($criterion == 'photo' || $criterion == 'illustration' ||  $criterion == 'vector') {
            $args = $photo->criterionType($criterion);
        }
        elseif ($criterion == 'horizontal' || $criterion == 'vertical') {
            $args = $photo->criterionOrientation($criterion);
        }
        elseif ($criterion = 'price-asc') {
            $args = $photo->criterionPriceAsc($criterion);
        }
        elseif ($criterion = 'price-desc') {
            $args = $photo->criterionPriceDesc($criterion);
        }
        $tags = $photo->randomTags();
        
        return $this->render('criterion',$args, $tags);
    }
}