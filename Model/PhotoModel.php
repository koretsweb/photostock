<?php
class PhotoModel extends DbConnection 
{
    public function showAll()
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }

    public function randomTags()
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT title, id FROM all_photos ORDER BY RAND() LIMIT 10");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }

    public function showCat($cat)
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos WHERE category='{$cat[2]}'");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }

    public function showLasts()
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos ORDER BY id DESC LIMIT 6");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }

    public function showOne($id)
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos WHERE id = '{$id}'");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }

    public function criterionType($criterion)
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos WHERE type='{$criterion}'");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }
    public function criterionOrientation($criterion)
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos WHERE orientation='{$criterion}'");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }
    public function criterionPriceAsc()
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos ORDER BY price ASC");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }
    public function criterionPriceDesc()
    {
        $db = DbConnection::getInstance()->getpdo();
        $sth = $db->query("SELECT * FROM all_photos ORDER BY price DESC");
        $sth = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $sth;
    }
}