-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 20 2016 г., 19:00
-- Версия сервера: 10.1.10-MariaDB
-- Версия PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `photo_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `all_photos`
--

CREATE TABLE `all_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) NOT NULL,
  `author` text NOT NULL,
  `price` float UNSIGNED NOT NULL,
  `show_dir` varchar(250) NOT NULL,
  `dir` varchar(250) NOT NULL,
  `category` varchar(250) NOT NULL,
  `type` varchar(100) NOT NULL,
  `orientation` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `all_photos`
--

INSERT INTO `all_photos` (`id`, `title`, `author`, `price`, `show_dir`, `dir`, `category`, `type`, `orientation`) VALUES
(37, 'Косуля', '', 0.2, './uploads/roe-deer-1482712_1280marked.jpg', './uploads/roe-deer-1482712_1920.jpg', 'animals', 'photo', 'vertical'),
(38, 'Попкорн', 'Miruams', 1, './uploads/popcorn-1433327_1920marked.jpg', './uploads/popcorn-1433327_1920.jpg', 'food', 'photo', 'horizontal'),
(39, 'Орёл', '', 1, './uploads/seagull-1511862_1920marked.jpg', './uploads/seagull-1511862_1920.jpg', 'animals', 'photo', 'vertical'),
(40, 'Малина', '', 0.65, './uploads/berries-1493905_1920marked.jpg', './uploads/berries-1493905_1920.jpg', 'food', 'photo', 'vertical'),
(41, 'Рассвет', '', 0.54, './uploads/sunset-1365830_1920marked.jpg', './uploads/sunset-1365830_1920.jpg', 'nature', 'photo', 'vertical'),
(42, 'Зажигалка', 'Thinex', 0.58, './uploads/lighter-1363095_1920marked.jpg', './uploads/lighter-1363095_1920.jpg', 'other', 'photo', 'horizontal'),
(43, 'Огонь', '', 0.4, './uploads/fire-marked.jpg', './uploads/fire-227291_1920.jpg', 'nature', 'photo', 'vertical'),
(44, 'Колокольчики', 'Marisa04', 1, './uploads/bluebells-1429817_1280marked.jpg', './uploads/bluebells-1429817_1280.jpg', 'nature', 'photo', 'vertical'),
(45, 'Кот', 'allyartist', 1, './uploads/cat-1508613_1920marked.jpg', './uploads/cat-1508613_1920.jpg', 'animals', 'photo', 'vertical'),
(46, 'Лошадь', '', 0.75, './uploads/horses-1414889_1920marked.jpg', './uploads/horses-1414889_1920.jpg', 'animals', 'photo', 'vertical'),
(47, 'Бейсбол', 'Miruams', 0.55, './uploads/baseball-team-1526701_1280marked.jpg', './uploads/baseball-team-1526701_1280.jpg', 'sport', 'photo', 'horizontal'),
(48, 'Собака', '', 0.42, './uploads/english-733876marked.png', './uploads/english-733876.png', 'animals', 'vector', 'vertical'),
(49, 'Лягушка', '', 0.4, './uploads/frog-1495034_1920marked.jpg', './uploads/frog-1495034_1920.jpg', 'nature', 'photo', 'horizontal'),
(50, 'Компьютер', '', 0.65, './uploads/notebook-871056marked.png', './uploads/notebook-871056.png', 'computers', 'vector', 'vertical'),
(51, 'Кости', '', 0.5, './uploads/roll-the-dice-1502706_1920marked.jpg', './uploads/roll-the-dice-1502706_1920.jpg', 'other', 'vector', 'horizontal'),
(52, 'Кости', '', 0.3, './uploads/dice-575658_1280marked.png', './uploads/dice-575658_1280.png', 'other', 'vector', 'vertical'),
(53, 'Банан', '', 0.25, './uploads/bananas-575773marked.png', './uploads/bananas-575773.png', 'food', 'vector', 'vertical'),
(54, 'Восход', '', 1.2, './uploads/merchant-pull-1398066_1920marked.jpg', './uploads/merchant-pull-1398066_1920.jpg', 'nature', 'illustration', 'horizontal'),
(55, 'Деньги', '', 0.3, './uploads/money-576443_1280marked.png', './uploads/money-576443_1280.png', 'business', 'vector', 'vertical'),
(56, 'Море', '', 1.3, './uploads/ship-1366926_1920marked.jpg', './uploads/ship-1366926_1920.jpg', 'nature', 'illustration', 'horizontal'),
(57, 'Замок', '', 0.45, './uploads/castle-1510196_1280marked.jpg', './uploads/castle-1510196_1280.jpg', 'architecture', 'photo', 'vertical'),
(58, 'Программирование', '', 0.6, './uploads/computer-1209641_1920marked.jpg', './uploads/computer-1209641_1920.jpg', 'tech', 'photo', 'vertical'),
(59, 'Лампа', '', 1.2, './uploads/lamp-1475979_1920marked.jpg', './uploads/lamp-1475979_1920.jpg', 'tech', 'photo', 'vertical'),
(60, 'Аудиостудия', '', 0.34, './uploads/soundboard-1209885_1920marked.jpg', './uploads/soundboard-1209885_1920.jpg', 'tech', 'photo', 'vertical'),
(61, 'Чип', 'Melissa', 0.4, './uploads/tech-1495181_1920marked.jpg', './uploads/tech-1495181_1920.jpg', 'tech', 'photo', 'vertical'),
(62, 'Футбол', '', 0.4, './uploads/american-football-1474557_1280marked.jpg', './uploads/american-football-1474557_1280.jpg', 'sport', 'photo', 'vertical'),
(63, 'Тенис', '', 0.5, './uploads/badminton-1428046_1920marked.jpg', './uploads/badminton-1428046_1920.jpg', 'sport', 'photo', 'vertical'),
(64, 'Футбол', '', 0.2, './uploads/2016-1494716_1920marked.jpg', './uploads/2016-1494716_1920.jpg', 'sport', 'photo', 'horizontal'),
(69, 'Книга', '', 0.4, './uploads/knowledge-1052014_1920marked.jpg', './uploads/knowledge-1052014_1920.jpg', 'education', 'photo', 'vertical'),
(70, 'Доска', '', 0.4, './uploads/mathematics-1509559_1920marked.jpg', './uploads/mathematics-1509559_1920.jpg', 'education', 'photo', 'vertical'),
(71, 'Доска', 'Melissa', 0.2, './uploads/board-1514165_1920marked.jpg', './uploads/board-1514165_1920.jpg', 'education', 'photo', 'vertical'),
(72, 'Тетрадь', '', 0.4, './uploads/pencil-918449_1920markled.jpg', './uploads/pencil-918449_1920.jpg', 'education', 'photo', 'vertical'),
(74, 'Музыка', '', 0.4, './uploads/turntable-1337986_1280marked.jpg', './uploads/turntable-1337986_1280.jpg', 'music', 'photo', 'vertical'),
(75, 'Улыбка', '', 0.15, './uploads/urban-1513868_1920marked.jpg', './uploads/urban-1513868_1920.jpg', 'emotions', 'photo', 'vertical'),
(76, 'Микрофон', '', 0.27, './uploads/micro-1494436_192marked.jpg', './uploads/micro-1494436_1920.jpg', 'music', 'photo', 'vertical');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `all_photos`
--
ALTER TABLE `all_photos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `all_photos`
--
ALTER TABLE `all_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
